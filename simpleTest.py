# Copyright 2006-2015 Coppelia Robotics GmbH. All rights reserved. 
# marc@coppeliarobotics.com
# www.coppeliarobotics.com
# 
# -------------------------------------------------------------------
# THIS FILE IS DISTRIBUTED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
# WARRANTY. THE USER WILL USE IT AT HIS/HER OWN RISK. THE ORIGINAL
# AUTHORS AND COPPELIA ROBOTICS GMBH WILL NOT BE LIABLE FOR DATA LOSS,
# DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS WHILE USING OR
# MISUSING THIS SOFTWARE.
# 
# You are free to use/modify/distribute this file for whatever purpose!
# -------------------------------------------------------------------
#
# This file was automatically created for V-REP release V3.2.3 rev4 on December 21st 2015

# Make sure to have the server side running in V-REP: 
# in a child script of a V-REP scene, add following command
# to be executed just once, at simulation start:
#
# simExtRemoteApiStart(19999)
#
# then start simulation, and run this program.
#
# IMPORTANT: for each successful call to simxStart, there
# should be a corresponding call to simxFinish at the end!



# returns the handle for a given name in the scene
# the api call for this feels harder to use, not like we'll have 1000s of objects anyways
def getObjectHandle(objectName):
     #get a bunch of data about the scene
    returnCode, handles, intData, floatData, stringData = vrep.simxGetObjectGroupData(clientID, vrep.sim_appobj_object_type, 0, vrep.simx_opmode_oneshot_wait)
    for i in range(len(handles)):
        if stringData[i] == objectName:
            return i
    return -1

def getObjectOrientation(objectHandle, relativeTo = -1):
    returnCode, eulerAngles = vrep.simxGetObjectOrientation(clientID, objectHandle, relativeTo, vrep.simx_opmode_oneshot_wait)
    if returnCode==vrep.simx_return_ok:
        return eulerAngles
    else:
        print 'Can not retrieve object orientation for handle:'+str(objectHandle)+'  returnCode=' + str(returnCode)
        return 'e','e','e'
        
def getObjectPosition(objectHandle, relativeTo = -1):
    returnCode, pos = vrep.simxGetObjectPosition(clientID, objectHandle, relativeTo, vrep.simx_opmode_oneshot_wait)
    if returnCode==vrep.simx_return_ok:
        return pos
    else:
        print 'Can not retrieve object position for handle:'+str(objectHandle)+'  returnCode=' + str(returnCode)
        return 'e','e','e'

#pause = boolean, true-paused, false-resumed, might be useful to set multiple values at once
def pauseComm(pause):
    vrep.simxPauseCommunication(clientID, pause)

try:
    import vrep
except:
    print ('--------------------------------------------------------------')
    print ('"vrep.py" could not be imported. This means very probably that')
    print ('either "vrep.py" or the remoteApi library could not be found.')
    print ('Make sure both are in the same folder as this file,')
    print ('or appropriately adjust the file "vrep.py"')
    print ('--------------------------------------------------------------')
    print ('')

import time

print ('Program started')
vrep.simxFinish(-1) # just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP
if clientID!=-1:
    print ('Connected to remote API server')

    # Now try to retrieve data in a blocking fashion (i.e. a service call):
    res,objs=vrep.simxGetObjects(clientID,vrep.sim_handle_all,vrep.simx_opmode_oneshot_wait)
    if res==vrep.simx_return_ok:
        print ('Number of objects in the scene: ',len(objs))
    else:
        print ('Remote API function call returned with error code: ',res)

    time.sleep(2)

    # Now retrieve streaming data (i.e. in a non-blocking fashion):
    #startTime=time.time()
    #vrep.simxGetIntegerParameter(clientID,vrep.sim_intparam_mouse_x,vrep.simx_opmode_streaming) # Initialize streaming
    #while time.time()-startTime < 5:
    #    returnCode,data=vrep.simxGetIntegerParameter(clientID,vrep.sim_intparam_mouse_x,vrep.simx_opmode_buffer) # Try to retrieve the streamed data
    #    if returnCode==vrep.simx_return_ok: # After initialization of streaming, it will take a few ms before the first value arrives, so check the return code
    #        print ('Mouse position x: ',data) # Mouse position x is actualized when the cursor is over V-REP's window
    #    time.sleep(0.005)

    robotHandle = getObjectHandle("robot")
    print 'robot handle:' + str(robotHandle)
    robotOri = getObjectOrientation(robotHandle)
    print 'robotOri:' + str(robotOri)
    robotPos = getObjectPosition(robotHandle)
    print 'robotPos:' + str(robotOri)
    
    print vrep.simxSetObjectFloatParameter(clientID, robotHandle, 11, 1, vrep.simx_opmode_oneshot_wait)
    

    # Now send some data to V-REP in a non-blocking fashion:
    vrep.simxAddStatusbarMessage(clientID,'Hello V-REP!',vrep.simx_opmode_oneshot)

    # Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
    vrep.simxGetPingTime(clientID)

    # Now close the connection to V-REP:
    vrep.simxFinish(clientID)
else:
    print ('Failed connecting to remote API server')
print ('Program ended')
